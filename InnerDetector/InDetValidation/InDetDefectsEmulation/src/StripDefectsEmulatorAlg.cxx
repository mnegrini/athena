/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "StripDefectsEmulatorAlg.h"

namespace InDet {
   const std::vector<int> DefectsEmulatorTraits<SCT_RDO_Container>::IDAdapter::s_dummyvector;
}
